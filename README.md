# Description de CALC_CHAMP_COMB
CALC_CHAMP_COMB permet de réaliser des combinaisons de chargement depuis un résultat MULTI_ELAS (issu de MACRO_ELAS_MULT).
Cela permet par exemple de réaliser un calcul avec les cas de chargement suivant :

- PP = Chargement dû au poids propres
- VENT = Chargement dû aux dynamiques du vent

Puis de calculer des résultats combinés issus de ces chargements élémentaires :

`COMBI_1 = 1.5*PP + 1*VENT`

`COMBI_2 = 1*PP + 0.5*VENT`

Au final, la macro ajoute les cas de charge combinés dans la structure résultat de sortie. Pour y accéder, il suffit d'utiliser le nom fourni dans le mot clés NOM_COMB (cf. Exemple ci-dessous)

# Exemple d'utilisation

```python
RESU = MACRO_ELAS_MULT(MODELE=MO,
                       CHAM_MATER=CHMAT,
                       CARA_ELEM=CARELEM,
                       CHAR_MECA_GLOBAL=CHLIM,
                       CAS_CHARGE=(_F(NOM_CAS='PP',
                                      CHAR_MECA=CH_PP,),
                                   _F(NOM_CAS='VENT',
                                      CHAR_MECA=CH_VENT,),),)
                                      
RES_COMB = CALC_CHAMP_COMB(RESULTAT=RESU,
                           TOUT='OUI',
                           FORCE='REAC_NODA',
                           LIST_COMB=(
                               _F(NOM_COMB='1.5*PP + 1*VENT',
                                  COMB=(_F(NOM_CAS='PP', COEF_R=1.5),
                                        _F(NOM_CAS='VENT', COEF_R=1),)
                                  ),
                               _F(NOM_COMB='1*PP + 0.5*VENT',
                                  COMB=(_F(NOM_CAS='PP', COEF_R=1),
                                        _F(NOM_CAS='VENT', COEF_R=0.5),)
                                  ),
                           ))
                           
IMPR_RESU(FORMAT='RESULTAT',
          UNITE=8,
          RESU=_F(RESULTAT=RES_COMB,
                  NOM_CHAM='REAC_NODA',
                  NOM_CAS='1*PP + 0.5*VENT',
                  FORM_TABL='EXCEL',
                  TOUT_CMP='OUI',
                  GROUP_NO='N_BLOQ',),)
```
# Données d'entrées de CALC_CHAMP_COMB
## Données identique à CALC_CHAMP :
1. RESULTAT
2. reuse
3. TOUT / GROUP_MA / MAILLE
4. FORCE
5. CRITERES
6. CONTRAINTE
7. ENERGIE
8. VARI_INTERNE
9. DEFORMATION

## Données spécifiques:
- LIST_COMB : tuple contenant les combinaisons, celles-ci sont composées de :
  - NOM_COMB : Nom à donner à la combinaison
  - COMB : tuple des combinaisons élémentaires (facteur) composant la combinaison, celles-ci sont composées de :
    - NOM_CAS : Nom du cas de charge
    - COEF_R : Coefficient à appliquer sur ce cas de charge

# Installation de la macro CALC_CHAMP_COMB

1. Déplacer le fichier `calc_champ_comb_ops.py` dans le répertoire : `$CODE_ASTER_ROOT/$VERSION/lib/aster/Macro`
2. Copier le contenu du fichier `calc_champ_comb.capy` dans le fichier : `$CODE_ASTER_ROOT/$VERSION/lib/aster/Cata/cata.py`

Fini !
La macro CALC_CHAMP_COMB est prête à être utiliser dans les fichiers de commande
