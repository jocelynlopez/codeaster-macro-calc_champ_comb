# coding=utf-8
# COPYRIGHT (C) 2016 - Jocelyn LOPEZ - jocelyn.lopez.pro@gmail.com

from Cata.cata import *


def find_type_cham(nom_cham):

    NOM_CHAM_TO_TYPE_CHAM = {
        'ACCE': 'NOEU_DEPL_R',
        'ETOT_ELEM': 'ELEM_ENER_R',
        'ACCE_ABSOLU': 'NOEU_DEPL_R',
        'ETOT_ELGA': 'ELGA_ENER_R',
        'COHE_ELEM': 'ELEM_NEUT_R',
        'ETOT_ELNO': 'ELGA_ENER_R',
        'COMPORTEMENT': 'CART_COMPOR',
        'FERRAILLAGE': 'ELEM_FER2_R',
        'COMPORTHER': 'CART_COMPOR',
        'FLHN_ELGA': 'ELGA_FLHN_R',
        'DEGE_ELGA': 'ELGA_EPSI_R',
        'FLUX_ELGA': 'ELGA_FLUX_R',
        'DEGE_ELNO': 'ELNO_EPSI_R',
        'FLUX_ELNO': 'ELNO_FLUX_R',
        'DEGE_NOEU': 'NOEU_EPSI_R',
        'FLUX_NOEU': 'NOEU_FLUX_R',
        'DEPL': 'NOEU_DEPL_R',
        'FORC_AMOR': 'NOEU_DEPL_R',
        'DEPL_ABSOLU': 'NOEU_DEPL_R',
        'FORC_EXTE': 'NOEU_DEPL_R',
        'DEPL_VIBR': 'NOEU_DEPL_R',
        'FORC_LIAI': 'NOEU_DEPL_R',
        'DERA_ELGA': 'ELGA_DERA_R',
        'FORC_NODA': 'NOEU_DEPL_R',
        'DERA_ELNO': 'ELNO_DERA_R',
        'GEOMETRIE': 'NOEU_GEOM_R',
        'DERA_NOEU': 'NOEU_DERA_R',
        'HYDR_ELNO': 'ELNO_HYDR_R',
        'DISS_ELEM': 'ELEM_DISS_R',
        'HYDR_NOEU': 'NOEU_HYDR_R',
        'DISS_ELGA': 'ELGA_DISS_R',
        'INDC_ELEM': 'ELEM_NEUT_I',
        'DISS_ELNO': 'ELNO_DISS_R',
        'INDL_ELGA': 'ELGA_INDL_R',
        'DISS_NOEU': 'NOEU_DISS_R',
        'INTE_ELNO': 'ELNO_INTE_R',
        'DIVU': 'NOEU_EPSI_R',
        'INTE_NOEU': 'NOEU_INTE_R',
        'DURT_ELNO': 'ELNO_DURT_R',
        'IRRA': 'NOEU_IRRA_R',
        'DURT_NOEU': 'NOEU_DURT_R',
        'META_ELNO': 'ELNO_VARI_R',
        'ECIN_ELEM': 'ELEM_ENER_R',
        'META_NOEU': 'NOEU_VARI_R',
        'EFGE_ELGA': 'ELGA_SIEF_R',
        'MODE_FLAMB': 'NOEU_DEPL_R',
        'EFGE_ELNO': 'ELNO_SIEF_R',
        'MODE_STAB': 'NOEU_DEPL_R',
        'EFGE_NOEU': 'NOEU_SIEF_R',
        'NEUT': 'NOEU_NEUT_R',
        'ENDO_ELGA': 'ELGA_SIEF_R',
        'PDIL_ELGA': 'ELGA_PDIL_R',
        'ENDO_ELNO': 'NOEU_SIEF_R',
        'PRAC_ELNO': 'ELNO_PRAC_R',
        'ENDO_NOEU': 'ELNO_SIEF_R',
        'PRAC_NOEU': 'NOEU_PRAC_R',
        'ENEL_ELEM': 'ELEM_ENER_R',
        'PRES': 'NOEU_PRES_C',
        'ENEL_ELGA': 'ELGA_ENER_R',
        'PRME_ELNO': 'ELNO_PRME_R',
        'ENEL_ELNO': 'ELNO_ENER_R',
        'PTOT': 'NOEU_DEPL_R',
        'ENEL_NOEU': 'NOEU_ENER_R',
        'QIRE_ELEM': 'ELEM_ERRE_R',
        'EPEQ_ELGA': 'ELGA_EPSI_R',
        'QIRE_ELNO': 'ELNO_ERRE_R',
        'EPEQ_ELNO': 'ELNO_EPSI_R',
        'QIRE_NOEU': 'NOEU_ERRE_R',
        'EPEQ_NOEU': 'NOEU_EPSI_R',
        'QIZ1_ELEM': 'ELEM_ERRE_R',
        'EPFD_ELGA': 'ELGA_EPSI_R',
        'QIZ2_ELEM': 'ELEM_ERRE_R',
        'EPFD_ELNO': 'ELNO_EPSI_R',
        'REAC_NODA': 'NOEU_DEPL_R',
        'EPFD_NOEU': 'NOEU_EPSI_R',
        'SECO_ELEM': 'ELEM_NEUT_R',
        'EPFP_ELGA': 'ELGA_EPSI_R',
        'SIEF_ELGA': 'ELGA_SIEF_R',
        'EPFP_ELNO': 'ELNO_EPSI_R',
        'SIEF_ELNO': 'ELNO_SIEF_R',
        'EPFP_NOEU': 'NOEU_EPSI_R',
        'SIEF_NOEU': 'NOEU_SIEF_R',
        'EPME_ELGA': 'ELGA_EPSI_R',
        'SIEQ_ELGA': 'ELGA_SIEF_R',
        'EPME_ELNO': 'ELNO_EPSI_R',
        'SIEQ_ELNO': 'ELNO_SIEF_R',
        'EPMG_ELGA': 'ELGA_EPSI_R',
        'SIEQ_NOEU': 'NOEU_SIEF_R',
        'EPMG_ELNO': 'ELNO_EPSI_R',
        'SIGM_ELGA': 'ELGA_SIEF_R',
        'EPMG_NOEU': 'NOEU_EPSI_R',
        'SIGM_ELNO': 'ELNO_SIEF_R',
        'EPMQ_ELGA': 'ELGA_EPSI_R',
        'SIGM_NOEU': 'NOEU_SIEF_R',
        'EPMQ_ELNO': 'ELNO_EPSI_R',
        'SING_ELEM': 'ELEM_SING_R',
        'EPMQ_NOEU': 'NOEU_EPSI_R',
        'SING_ELNO': 'ELNO_SING_R',
        'EPOT_ELEM': 'ELEM_ENER_R',
        'SIPM_ELNO': 'ELNO_SIEF_R',
        'EPSA_ELNO': 'ELNO_EPSI_R',
        'SIPO_ELNO': 'ELNO_SIEF_R',
        'EPSA_NOEU': 'NOEU_EPSI_R',
        'SIPO_NOEU': 'NOEU_SIEF_R',
        'EPSG_ELGA': 'ELGA_EPSI_R',
        'SIRO_ELEM': 'ELEM_SIEF_R',
        'EPSG_ELNO': 'ELNO_EPSI_R',
        'SISE_ELNO': 'ELNO_SIEF_R',
        'EPSG_NOEU': 'NOEU_EPSI_R',
        'SIZ1_NOEU': 'NOEU_SIEF_R',
        'EPSI_ELGA': 'ELGA_EPSI_R',
        'SIZ2_NOEU': 'NOEU_SIEF_R',
        'EPSI_ELNO': 'ELNO_EPSI_R',
        'SOUR_ELGA': 'ELGA_SOUR_R',
        'EPSI_NOEU': 'NOEU_EPSI_R',
        'STRX_ELGA': 'ELGA_STRX_R',
        'EPSP_ELGA': 'ELGA_EPSI_R',
        'TEMP': 'NOEU_TEMP_R',
        'EPSP_ELNO': 'ELNO_EPSI_R',
        'THETA': 'NOEU_DEPL_R',
        'EPSP_NOEU': 'NOEU_EPSI_R',
        'UTXX_ELGA': 'Voir remarque ci-dessous',
        'EPVC_ELGA': 'ELGA_EPSI_R',
        'UTXX_ELNO': 'Voir remarque ci-dessous',
        'EPVC_ELNO': 'ELNO_EPSI_R',
        'UTXX_NOEU': 'Voir remarque ci-dessous',
        'EPVC_NOEU': 'NOEU_EPSI_R',
        'VAEX_ELGA': 'ELGA_NEUT_R',
        'ERME_ELEM': 'ELEM_ERRE_R',
        'VAEX_ELNO': 'ELNO_NEUT_R',
        'ERME_ELNO': 'ELNO_ERRE_R',
        'VAEX_NOEU': 'NOEU_NEUT_R',
        'ERME_NOEU': 'NOEU_ERRE_R',
        'VALE_CONT': 'NOEU_INFC_R',
        'ERTH_ELEM': 'ELEM_ERRE_R',
        'VARC_ELGA': 'ELGA_VARC_R',
        'ERTH_ELNO': 'ELNO_ERRE_R',
        'VARI_ELGA': 'ELGA_VARI_R',
        'ERTH_NOEU': 'NOEU_ERRE_R',
        'VARI_ELNO': 'ELNO_VARI_R',
        'ERZ1_ELEM': 'ELEM_ERRE_R',
        'VARI_NOEU': 'NOEU_VAR2_R',
        'ERZ2_ELEM': 'ELEM_ERRE_R',
        'VITE': 'NOEU_DEPL_R',
        'ETHE_ELEM': 'ELEM_ENER_R',
        'VITE_ABSOLU': 'NOEU_DEPL_R',
    }

    if nom_cham.startswith('UT'):
        raise NotImplementedError("UTXX_ELGA, UTXX_ELNO, UTXX_NOEU ne sont pas prise en charge")
    else:
        return NOM_CHAM_TO_TYPE_CHAM[nom_cham]


def calc_champ_comb_ops(self, RESULTAT, LIST_COMB, **args):
    """Ecriture de la macro CALC_CHAMP_COMB."""
    ier = 0
    from Accas import _F

    # On importe les definitions des commandes a utiliser dans la macro
    CALC_CHAMP = self.get_cmd('CALC_CHAMP')
    CREA_CHAMP = self.get_cmd('CREA_CHAMP')
    CREA_RESU = self.get_cmd('CREA_RESU')

    # La macro compte pour 1 dans la numerotation des commandes
    self.set_icmd(1)

    # Le concept sortant de type mult_elas est nommé
    # 'RESU_COMB' dans le contexte de la macro si pas de reuse
    if self.reuse is not None:
        self.DeclareOut('RESULTAT', self.sd)
    else:
        self.DeclareOut('RESU_COMB', self.sd)

    # --------------------------------------------------------------------
    #                       Corps de la macro
    # --------------------------------------------------------------------
    # Détermination de la liste des champs à traiter
    NOM_CHAM = tuple()
    if args['FORCE']:
        NOM_CHAM += args['FORCE']
    if args['CRITERES']:
        NOM_CHAM += args['CRITERES']
    if args['CONTRAINTE']:
        NOM_CHAM += args['CONTRAINTE']
    if args['ENERGIE']:
        NOM_CHAM += args['ENERGIE']
    if args['VARI_INTERNE']:
        NOM_CHAM += args['VARI_INTERNE']
    if args['DEFORMATION']:
        NOM_CHAM += args['DEFORMATION']

    # Détermination de la liste global des cas de charge
    l_global_CAS = []
    for idx_comb, item in enumerate(LIST_COMB):
        COMB = item['COMB']
        l_global_CAS += [e['NOM_CAS'] for e in COMB]
    l_global_CAS = list(set(l_global_CAS))

    # Calcul des champs
    for NOM_CAS in l_global_CAS:
        if self.reuse is not None:
            RESULTAT = CALC_CHAMP(reuse=RESULTAT,
                                  RESULTAT=RESULTAT,
                                  NOM_CAS=NOM_CAS,
                                  **args)
        else:
            __RESU_TMP = RESULTAT
            __RESU_TMP = CALC_CHAMP(reuse=__RESU_TMP,
                                    RESULTAT=RESULTAT,
                                    NOM_CAS=NOM_CAS,
                                    **args)

    # Creation de l'ensemble des combinaisons
    COMBINAISON_GLOBAL = tuple()
    for idx_cham, NOM_CHAM_COURANT in enumerate(NOM_CHAM):

        # Determination du type de champ
        TYPE_CHAM_COURANT = find_type_cham(NOM_CHAM_COURANT)

        # Boucle sur les combinaisons
        for idx_comb, item in enumerate(LIST_COMB):

            COMB = item['COMB']
            l_CAS = [e['NOM_CAS'] for e in COMB]
            COMBINAISON_LOCAL = tuple()

            # Création des champs pour chaque cas de charge
            for idx, NOM_CAS in enumerate(l_CAS):

                if self.reuse is not None:
                    __CHAMPS = CREA_CHAMP(OPERATION='EXTR',
                                          TYPE_CHAM=TYPE_CHAM_COURANT,
                                          RESULTAT=RESULTAT,
                                          NOM_CHAM=NOM_CHAM_COURANT,
                                          NOM_CAS=NOM_CAS,)
                else:
                    __CHAMPS = CREA_CHAMP(OPERATION='EXTR',
                                          TYPE_CHAM=TYPE_CHAM_COURANT,
                                          RESULTAT=__RESU_TMP,
                                          NOM_CHAM=NOM_CHAM_COURANT,
                                          NOM_CAS=NOM_CAS,)

                COMBINAISON_LOCAL += (_F(CHAM_GD=__CHAMPS, COEF_R=COMB[idx]['COEF_R']),)
                # COMBINAISON_LOCAL += (_F(TOUT='OUI', CHAM_GD=__CHAMPS,
                #                          COEF_R=COMB[idx]['COEF_R'], CUMUL='OUI'),)
                
            __CHACOM = CREA_CHAMP(OPERATION='COMB',
                                  TYPE_CHAM=TYPE_CHAM_COURANT,
                                  COMB=COMBINAISON_LOCAL,)
            # __CHACOM = CREA_CHAMP(OPERATION='ASSE',
            #                       TYPE_CHAM=TYPE_CHAM_COURANT,
            #                       ASSE=COMBINAISON_LOCAL,)
            
            COMBINAISON_GLOBAL += (_F(CHAM_GD=__CHACOM, NOM_CAS=LIST_COMB[idx_comb]['NOM_COMB']),)

    # Creation du resultat combiné
    if self.reuse is not None:
        RESULTAT = CREA_RESU(reuse=RESULTAT,
                             OPERATION='AFFE',
                             TYPE_RESU='MULT_ELAS',
                             NOM_CHAM=NOM_CHAM_COURANT,
                             AFFE=COMBINAISON_GLOBAL,)
    else:
        RESU_COMB = CREA_RESU(OPERATION='AFFE',
                              TYPE_RESU='MULT_ELAS',
                              NOM_CHAM=NOM_CHAM_COURANT,
                              AFFE=COMBINAISON_GLOBAL,)

    # --------------------------------------------------------------------

    return ier
